#include "MyForm.h"

[STAThreadAttribute]
void Main(array<String^>^ args) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	
//#ifdef _DEBUG
//	System::String ^exeFullPathName = gcnew System::String("C:\\VersionControlCppClrStationSample-64bit\\VersionControlCppClrStationSample.exe");
//	System::String ^repoPath = gcnew System::String(Path::GetDirectoryName(exeFullPathName));// + "\\";
//	System::String ^returnString = "";
//	bool autoUpdateToServerLatest = false;//true
//	System::String ^token = "";// "sample token";
//	VersionControl::NET::Dll^ dll = gcnew VersionControl::NET::Dll();
//	dll->CheckProgramVersion_FailingThat_ResetBeforeRestart(
//		exeFullPathName,
//		repoPath,
//		returnString,
//		token,
//		"");
//#else
//	System::String ^exeFullPathName = gcnew System::String(Application::ExecutablePath);
//	System::String ^repoPath = gcnew System::String(Path::GetDirectoryName(exeFullPathName));// + "\\";
//	System::String ^returnString = "";
//	bool autoUpdateToServerLatest = false;//true
//	System::String ^token = "";// "sample token";
//	VersionControl::NET::Dll^ dll = gcnew VersionControl::NET::Dll();
//	dll->CheckProgramVersion_FailingThat_ResetBeforeRestart(
//		exeFullPathName,
//		repoPath,
//		returnString,
//		token,
//		"");
//#endif

	CClrProject::MyForm form;
	Application::Run(%form);
}
