#pragma once
#using <VersionControl.NET.dll>

using namespace VersionControl;
using namespace System;
using namespace System::Windows::Forms;
using namespace System::IO;

namespace CClrProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

#ifdef _DEBUG
			System::String ^exeFullPathName = gcnew System::String("C:\\VersionControlCppCliStationSample-64bit\\VersionControlCppCliStationSample.exe");
#else
			System::String ^exeFullPathName = gcnew System::String(Application::ExecutablePath);
#endif

			System::String ^repoPath = gcnew System::String(Path::GetDirectoryName(exeFullPathName));// + "\\";
			System::String ^returnString = "";
			//bool autoUpdateToServerLatest = false;//true
			System::String ^token = "";		// textBox_Token->Text;

			System::String^ lotId = "";		// textBox_LotID->Text;
			System::String^ recipeName = "";// textBox_RecipeName->Text;
			bool resumeWithPausedLot = false;
			bool displayBlockingMessageBeforeReset = true;
			System::String^ blockingMessage = "";
			bool resetBeforeMandatoryRestartWhenFailedCheck = true;

			VersionControl::NET::Dll^ dll = gcnew VersionControl::NET::Dll();

			dll->CheckProgramVersion(//_FailingThat_ResetBeforeRestart(
				exeFullPathName,
				repoPath,
				returnString,
				token,
				lotId,
				recipeName,
				resumeWithPausedLot,
				displayBlockingMessageBeforeReset,
				blockingMessage,
				resetBeforeMandatoryRestartWhenFailedCheck
			);

			//pin_ptr<const wchar_t> wch = PtrToStringChars(_returnString);
			//errno_t result = wcsncpy_s(returnString, RETURNSTRING_BUFFER_SIZE, wch, _TRUNCATE);
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox_LotID;
	private: System::Windows::Forms::TextBox^  textBox_RecipeName;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  button_OpenLot;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox_Token;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox_LotID = (gcnew System::Windows::Forms::TextBox());
			this->textBox_RecipeName = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->button_OpenLot = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox_Token = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(57, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(68, 25);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Lot ID";
			// 
			// textBox_LotID
			// 
			this->textBox_LotID->Location = System::Drawing::Point(142, 6);
			this->textBox_LotID->Name = L"textBox_LotID";
			this->textBox_LotID->Size = System::Drawing::Size(174, 31);
			this->textBox_LotID->TabIndex = 1;
			this->textBox_LotID->Text = L"Lot123";
			// 
			// textBox_RecipeName
			// 
			this->textBox_RecipeName->Location = System::Drawing::Point(142, 44);
			this->textBox_RecipeName->Name = L"textBox_RecipeName";
			this->textBox_RecipeName->Size = System::Drawing::Size(174, 31);
			this->textBox_RecipeName->TabIndex = 3;
			this->textBox_RecipeName->Text = L"Recipe456";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(57, 47);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(79, 25);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Recipe";
			// 
			// button_OpenLot
			// 
			this->button_OpenLot->Location = System::Drawing::Point(142, 157);
			this->button_OpenLot->Name = L"button_OpenLot";
			this->button_OpenLot->Size = System::Drawing::Size(174, 78);
			this->button_OpenLot->TabIndex = 4;
			this->button_OpenLot->Text = L"Open Lot";
			this->button_OpenLot->UseVisualStyleBackColor = true;
			this->button_OpenLot->Click += gcnew System::EventHandler(this, &MyForm::button_OpenLot_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(57, 85);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(72, 25);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Token";
			// 
			// textBox_Token
			// 
			this->textBox_Token->Location = System::Drawing::Point(142, 82);
			this->textBox_Token->Name = L"textBox_Token";
			this->textBox_Token->Size = System::Drawing::Size(174, 31);
			this->textBox_Token->TabIndex = 6;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(524, 399);
			this->Controls->Add(this->textBox_Token);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button_OpenLot);
			this->Controls->Add(this->textBox_RecipeName);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox_LotID);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button_OpenLot_Click(System::Object^  sender, System::EventArgs^  e) {

#ifdef _DEBUG
		System::String ^exeFullPathName = gcnew System::String("C:\\VersionControlCppCliStationSample-64bit\\VersionControlCppCliStationSample.exe");
#else
		System::String ^exeFullPathName = gcnew System::String(Application::ExecutablePath);
#endif

		System::String ^repoPath = gcnew System::String(Path::GetDirectoryName(exeFullPathName));// + "\\";
		System::String ^returnString = "";
		//bool autoUpdateToServerLatest = false;//true
		System::String ^token = textBox_Token->Text;

		System::String^ lotId = textBox_LotID->Text;
		System::String^ recipeName = textBox_RecipeName->Text;
		bool resumeWithPausedLot = false;
		bool displayBlockingMessageBeforeReset = true;
		System::String^ blockingMessage = "";
		bool resetBeforeMandatoryRestartWhenFailedCheck = true;

		VersionControl::NET::Dll^ dll = gcnew VersionControl::NET::Dll();

		dll->CheckProgramVersion(//_FailingThat_ResetBeforeRestart(
			exeFullPathName,
			repoPath,
			returnString,
			token,
			lotId,
			recipeName,
			resumeWithPausedLot,
			displayBlockingMessageBeforeReset,
			blockingMessage,
			resetBeforeMandatoryRestartWhenFailedCheck
		);

		//pin_ptr<const wchar_t> wch = PtrToStringChars(_returnString);
		//errno_t result = wcsncpy_s(returnString, RETURNSTRING_BUFFER_SIZE, wch, _TRUNCATE);

	}
};
}
